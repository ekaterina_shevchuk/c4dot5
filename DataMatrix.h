#pragma once
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include "DataMatrix.h"
//#include "DecTree.h"
using namespace std;

class DataMatrix {
public:
	DataMatrix();
	~DataMatrix();
	DataMatrix(string dataFile);
	int Get_num_examples();
	int Get_num_atr();
	int Get_index_atr(string atr);
	vector <string> Get_example_class();
	vector<string> Get_atrs();
	vector<string> Get_value_atr(string name_atr);
	vector <string> Get_values_class_of_val_atr(string name_atr, string val_atr);
	void Delete_atr(int index);
	void Delete_rows_for_atr(string val_atr, string name_atr);
	string Get_atr_at_index(int index);
	string Get_val_class_at_val_atr(string name_atr, string val_atr);
	int num_atr = 0;
	int num_rows = 0;

private:
	vector<vector<string>> Matrix;

};