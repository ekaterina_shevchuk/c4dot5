#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include "DecTree.h"
#include "DataMatrix.h"


class DecTree {
public:
	string Node, Branch;
	vector <string> values;
	vector <DecTree*> Childs;
	bool* itsEnd;
	vector<string> value_class;
	DecTree();
	DecTree* Build_DecTree(DataMatrix matrix);
	void Print_DecTree(DecTree * tree);
	float Calc_Gain(DataMatrix matrix, string atr);
	float Calc_GainRatio(DataMatrix matrix, string atr);
	float Calc_SplitInfo(DataMatrix matrix, string atr);
	string Check_data(vector<string> names_atrs, vector<string> val_atrs);
};