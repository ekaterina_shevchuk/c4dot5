#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include "DataMatrix.h"
using namespace std;

DataMatrix::~DataMatrix() {
	for (int i = 0; i < Matrix.size(); i++)
		Matrix[i].erase(Matrix[i].begin(), Matrix[i].end());
	Matrix.erase(Matrix.begin(), Matrix.end());
}

DataMatrix::DataMatrix(string dataFile) {
	ifstream file;
	string line, elem;
	vector <string> Row;
	file.open(dataFile);
	if (file.is_open()) {
		while (getline(file, line)) {
			istringstream Iss(line);
			while (Iss.good()) {
				getline(Iss, elem, ' ');
				elem.erase(remove(elem.end() - 1, elem.end(), ' '), elem.end());
				Row.push_back(elem);
			}
			//�������, ������� ����� ��������� ������ Row	

			if (line.length()) {
				//cout << "()" << line.length() << endl;
				Matrix.push_back(Row);
				num_rows++;
				Row.erase(Row.begin(), Row.end());
			}
		}
	}
	num_atr = Matrix[0].size() - 1;
}

int DataMatrix::Get_num_atr() {
	return num_atr;
}

int DataMatrix::Get_num_examples() {
	return num_rows - 1;
}

//�������� ���������
vector<string> DataMatrix::Get_atrs() {
	return Matrix[0];
}

//�������� ����� �������� � ������� �� ��������
int DataMatrix::Get_index_atr(string atr) {
	int Index;
	for (int i = 0; i < DataMatrix::Get_num_atr(); i++) {
		if (Matrix[0][i] == atr) {
			Index = i;
			break;
		}
	}
	return Index;
}

//�������� �������� �� �������
string DataMatrix::Get_atr_at_index(int index) {
	vector<string> names_atrs = Get_atrs();
	return names_atrs[index];
}

//�������� ��� �������� �������� �� ��������
vector<string> DataMatrix::Get_value_atr(string name_atr) {
	vector <string> value;
	int Index = Get_index_atr(name_atr);
	for (int i = 0; i < Get_num_examples(); i++) {
		value.push_back(Matrix[i+1][Index]);
	}
	return value;
}

//�������� �������� ������ �� �������� ��������
string DataMatrix::Get_val_class_at_val_atr(string name_atr, string val_atr) {
	vector <string> val_class = Get_example_class(), val_atrs = Get_value_atr(name_atr);
	for (int i = 0; i < Get_num_examples(); i++) {
		if (val_atrs[i] == val_atr) {
			return (val_class[i]);
		}
	}
}

//�������� ������ �������� ������(��/���)
vector <string> DataMatrix::Get_example_class() {
	int num_class_inM = Get_num_atr();
	vector <string> values_class;
	for (int i = 1; i < Matrix.size(); i++) {
		values_class.push_back(Matrix[i][num_class_inM]);
	}
	return values_class;
}

//�������� ������ �������� ������ �� ������������� �������� �������� 
vector <string> DataMatrix::Get_values_class_of_val_atr(string name_atr, string val_atr) {
	vector <string> ret_val_class, val_class = Get_example_class(), val_atrs = Get_value_atr(name_atr);
	for (int i = 0; i < Get_num_examples(); i++) {
		if(val_atrs[i] == val_atr) {
			ret_val_class.push_back(val_class[i]);
		}
	}
	return ret_val_class;
}

void DataMatrix::Delete_atr(int index) {
	for (int i = 0; i < Matrix.size(); i++)
		Matrix[i].erase(Matrix[i].begin() + index);
	num_atr--;
}

void DataMatrix::Delete_rows_for_atr(string val_atr, string name_atr) {
	int index = Get_index_atr(name_atr);
	for (int i = 1; i < Matrix.size(); i++) {
		if (Matrix[i][index] != val_atr) {
			Matrix.erase(Matrix.begin() + i);
			num_rows--;
			i--;
		}
	}

}