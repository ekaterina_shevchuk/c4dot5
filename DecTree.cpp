#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <cstdlib>
#include <algorithm>
#include "DecTree.h"
using namespace std;

DecTree::DecTree() {
	Node = "";
	Branch = "";
}

vector <string> Get_uniq_value(vector<string> value) {
	vector<string> uniq_value;
	int choose = 1;
	for (int i = 0; i < value.size(); i++) {
		choose = 1;
		if (uniq_value.size() == 0) uniq_value.push_back(value[i]);
		else {
			for (int j = 0; j < uniq_value.size(); j++) {
				if (uniq_value[j] == value[i]){
					choose = 0;
					break;
				}
			}
			if (choose == 1) {
				uniq_value.push_back(value[i]);
			}
		}
	}
	return uniq_value;
}


int * Calc_score(vector<string> value, vector<string> uniq_value) {
	int *score = (int*)calloc(uniq_value.size()+1, sizeof(int));
	for (int i = 0; i < uniq_value.size(); i++) {
		for (int j = 0; j < value.size(); j++) {
			if (value[j] == uniq_value[i]) score[i]++;
		}
	}
	return score;
}

float DecTree::Calc_SplitInfo(DataMatrix matrix, string atr) {
	vector <string> value = matrix.Get_value_atr(atr);
	vector<string> uniq_value = Get_uniq_value(value);
	int *score = (int*)calloc(uniq_value.size(), sizeof(int));
	score = Calc_score(value, uniq_value);
	float SplitInfo = 0, tmp = 0;
	int size = value.size();
	for (int i = 0; i < uniq_value.size(); i++) {
		tmp = (float)score[i] / size;
		SplitInfo -= tmp * ((float)log(tmp)) / (log(2));
	}
	return SplitInfo;
}

float Calc_Entropy(vector <string> values_class) {
	vector <string> uniq_values_class = Get_uniq_value(values_class);
	int *score = Calc_score(values_class, uniq_values_class);
	float Entropy = 0, tmp = 0;
	int size = values_class.size();
	for (int i = 0; i < uniq_values_class.size(); i++) {
		tmp = ((float)score[i]) / size;
		Entropy -= tmp * ((float)log(tmp) / log(2));
	}
	return Entropy;
}

float DecTree::Calc_Gain(DataMatrix matrix, string atr) {
	vector <string> values_class = matrix.Get_example_class();
	vector <string> uniq_value_class = Get_uniq_value(values_class);
	float Entropy = Calc_Entropy(values_class);
	vector <string> value_atr = matrix.Get_value_atr(atr);
	vector <string> uniq_value_atr = Get_uniq_value(value_atr);
	int *score_uniq_atr = Calc_score(value_atr, uniq_value_atr);
	int tmp_size = matrix.Get_num_examples();
	float EntropyAtr = 0, sum_EntropyAtr = 0, tmp_entr = 0, tmp = 0;
	float Gain = Entropy;
	for (int i = 0; i < uniq_value_atr.size(); i++) {
		vector<string> val_class_of_val_atr = matrix.Get_values_class_of_val_atr(atr, uniq_value_atr[i]);
		int * score_uniq = Calc_score(val_class_of_val_atr, uniq_value_class);
		int size = 0;
		tmp_entr = 0;
		for (int i = 0; i < uniq_value_class.size(); i++) size += score_uniq[i];
		for (int j = 0; j < uniq_value_class.size(); j++) {
			tmp = ((float)score_uniq[j])/size;
			if (tmp == 0) {
				tmp_entr = 0;
			}
			else {
				tmp_entr -= tmp * ((float)log(tmp) / log(2));
			}
		}
		tmp = ((float)score_uniq_atr[i]) / tmp_size;
		Gain -= tmp * tmp_entr;
	}
	return Gain;
}

float DecTree::Calc_GainRatio(DataMatrix matrix, string atr) {
	return ((float)Calc_Gain(matrix, atr) / Calc_SplitInfo(matrix, atr));
}


DecTree* DecTree::Build_DecTree(DataMatrix matrix) {
	float main_GainRatio = 0, tmp_GainRatio = 0;
	string main_atr;
	vector <string> atrs = matrix.Get_atrs();

	for (int i = 0; i < matrix.Get_num_atr() ; i++) {
		tmp_GainRatio = Calc_GainRatio(matrix, atrs[i]);
		if (tmp_GainRatio > main_GainRatio) { //��� ����� ����� ������� �� �������
			main_GainRatio = tmp_GainRatio;
			main_atr = atrs[i];
		}
	}
	this->Node = main_atr;
	//������ ����� ������������ ��� ������ � ��������� ��� �����
	//���� ������� �������� �� main_uniq_value ����� ��������������� ������ �� Childs (��� �������)
	vector <string> main_uniq_values = Get_uniq_value(matrix.Get_value_atr(main_atr));
	this->values = main_uniq_values;
	int index = matrix.Get_index_atr(main_atr);
	itsEnd = (bool*)malloc(main_uniq_values.size() * sizeof(bool));
	this->value_class.reserve(main_uniq_values.size());
	vector<string> value_tmp(main_uniq_values.size());
	value_class = value_tmp;
	//��������� ���������� ������ ������
	if (((matrix.num_atr) - 1) > 1) { //���� ������� ���� �������, �� �� ����������� ������ �� ������� ��������
		for (int i = 0; i < main_uniq_values.size(); i++) {
			DataMatrix new_matrix = matrix;
			new_matrix.Delete_rows_for_atr(main_uniq_values[i], this->Node);
			new_matrix.Delete_atr(index);
			vector<string> values_class = new_matrix.Get_example_class();
			vector<string> uniq_value_class = Get_uniq_value(values_class);
			if (uniq_value_class.size() > 1) {
				//����� ������ �������� ������ ��� ����� �������� �������� � ����� ���������� �������� �� ����, ���� ���������� �������� >1, �� ������ ������, ����� ������� ����������� ����������, ��� ��� ����� � ���� �������� 
				//����� ���������� ��� sun ��� ������ �� �������, ������� �� Outlook = sun
				DecTree* new_tree = new DecTree();
				new_tree = new_tree->Build_DecTree(new_matrix);
				new_tree->Branch = main_uniq_values[i];
				Childs.push_back(new_tree);
			}
			else {
				itsEnd[i] = true;
				value_class[i] = uniq_value_class[0];
			}
		}
	}
	else {
		if (matrix.num_atr == 2) {
			vector<string> values_class = matrix.Get_example_class();
			vector<string> uniq_value_class = Get_uniq_value(values_class);
			
			if (uniq_value_class.size() == 1) {
				itsEnd[0] = true;
				value_class[0] = uniq_value_class[0];
			}
			else {
				for (int i = 0; i < main_uniq_values.size(); i++) {
					DecTree* new_tree = new DecTree();
					new_tree->Branch = main_uniq_values[i];
					new_tree->Node = matrix.Get_atr_at_index(1);
					DataMatrix new_matrix = matrix;
					new_matrix.Delete_rows_for_atr(main_uniq_values[i], this->Node);
					vector<string> values_all = new_matrix.Get_value_atr(new_tree->Node);
					new_tree->values = Get_uniq_value(values_all);
					vector<string> values_class_all = new_matrix.Get_example_class();
					for (int j = 0; j < new_tree->values.size(); j++) {
						new_tree->value_class.push_back(new_matrix.Get_val_class_at_val_atr(new_tree->Node, new_tree->values[j]));
					}
				}
			}
			}
		} 
		return this;
}


string DecTree::Check_data(vector<string> names_atrs, vector<string> val_atrs) {
	string answer;
	int index_atr;
	bool its_end = false;
	for (int i = 0; i < names_atrs.size(); i++) {
		if (this->Node == names_atrs[i]) {
			index_atr = i;
			names_atrs.erase(names_atrs.begin() + i);
			break;
		}
	}

	for (int i = 0; i < val_atrs.size(); i++) {
		if (this->values[i] == val_atrs[index_atr]) {
			val_atrs.erase(val_atrs.begin() + index_atr);
			index_atr = i;
			if (this->itsEnd[i] == true) {
				answer = this->value_class[i];
				its_end = true;
			}
			break;
		}
	}
	if (its_end == true)
		return answer;

	for (int i = 0; i < this->Childs.size(); i++) {
		if (this->Childs[i]->Branch == this->values[index_atr]) {
			index_atr = i;
			break;
		}
	}

	DecTree* new_tree = this->Childs[index_atr];
	answer = new_tree->Check_data(names_atrs, val_atrs);


	return answer;
}