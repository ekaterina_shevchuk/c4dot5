#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include "DecTree.h"
#include "DataMatrix.h"
using namespace std;


int main() {
	DataMatrix* matrix = new DataMatrix("data.txt");
	DecTree * tree = new DecTree();
	tree = tree->Build_DecTree(*matrix);
	string atr;
	vector<string> val_atrs, names_atrs;
	cin >> atr;
	while (atr != ".") {
		names_atrs.push_back(atr);
		cin >> atr;
	}
	cin >> atr;
	while (atr != ".") {
		val_atrs.push_back(atr);
		cin >> atr;
	}
	string answer = tree->Check_data(names_atrs, val_atrs);
	cout << endl << answer << endl;
	return 0;
}